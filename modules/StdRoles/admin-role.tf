#####Admin Role

resource "aws_iam_role" "admin_role" {
  name        = "role-${var.account}-admin"
  description = "cross account role for admin access to this account"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": {
      "AWS": "arn:aws:iam::${var.master_account_id}:root"
    },
    "Action": "sts:AssumeRole"
  }
}
EOF
}

#####Admin Policy

resource "aws_iam_role_policy" "admin_policy" {
  name = "test_policy"
  role = "${aws_iam_role.admin_role.id}"

  policy = "arn:aws:iam::aws:policy/AdministratorAccess"
}


