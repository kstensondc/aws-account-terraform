#####Admin Role

resource "aws_iam_role" "poweruser_role" {
  name        = "role-${var.account}-poweruser"
  description = "cross account role for poweruser access to this account"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": {
      "AWS": "arn:aws:iam::${var.master_account_id}:root"
    },
    "Action": "sts:AssumeRole"
  }
}
EOF
}

#####Admin Policy

resource "aws_iam_role_policy" "poweruser_policy" {
  name = "test_policy"
  role = "${aws_iam_role.poweruser_role.id}"

  policy = "arn:aws:iam::aws:policy/PowerUserAccess"
}
