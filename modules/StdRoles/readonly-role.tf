#####Admin Role

resource "aws_iam_role" "readonly_role" {
  name        = "role-${var.account}-readonly"
  description = "cross account role for readonly access to this account"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Principal": {
      "AWS": "arn:aws:iam::${var.master_account_id}:root"
    },
    "Action": "sts:AssumeRole"
  }
}
EOF
}

#####Admin Policy

resource "aws_iam_role_policy" "readonly_policy" {
  name = "test_policy"
  role = "${aws_iam_role.readonly_role.id}"

  policy = "arn:aws:iam::aws:policy/job-function/ViewOnlyAccess"
}
